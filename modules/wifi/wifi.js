const express = require('express');
const next = require('next');
const server = express()
const dev = process.env.NODE_ENV !== 'production';
const { exec, execSync, spawn } = require('child_process');
const EventEmitter = require("events").EventEmitter;
const crypto = require('crypto');
const hash = crypto.createHash('sha256');
const utf8 = require('utf8');
const dns = require('dns');
const bodyParser = require('body-parser');
const iwconfig = require('wireless-tools/iwconfig');
const iw = require('wireless-tools/iw');
const file = require('../file');
const hostapd = require('wireless-tools/hostapd');
const wpa_supplicant = require('wireless-tools/wpa_supplicant');
const path = require('path');
//const isOnline = require('is-online');
const internetAvailable = require("internet-available");
const app = next({ dev });
const handle = app.getRequestHandler();


const port = "80",
    ip = "10.10.10.10",
    netmask = "255.255.255.192",
    broadcast = "10.10.10.63",
    salt = "DevForceOne";
    


let displayHost, displayMac, serverlisten


class Wifi extends EventEmitter {

    
    constructor(){
        super();
        this._interface = 'wlan0';
        this._ssid = '';
        this._passphrase = '';
        this._enabled = false;
    }

    /*init(){
            let self = this
            return self._interface = 'wlan0'
    }*/

    generateSSID(){
        return new Promise((resolve, reject) => {
            exec('cat /sys/class/net/eth0/address', { shell: '/bin/bash' }, (errMac, mac) => {
                if (!errMac) {
                    let self = this
    
                    self._mac = mac.toUpperCase().trim();
                    hash.update(self._mac + salt);
                    var hashMac = hash.digest('hex');
                    // Options Hotspot
                    self._ssid = "Inovbox-" + hashMac.substr(0, 10),
                    self._passphrase = hashMac.substr(54, 10)
                    resolve({"ssid":self._ssid, "passphrase":self._passphrase})
                } else {
                    reject(errMac);
                }
            });
        })
    }

    /**
         * Active le Réseau Wifi de la box
         *  - scan des réseaux
         *  - désactiver wpa_supplicant (systemctl stop wpa_supplicant@wlan0)
         *  - Active l'ip
         *      - sudo ifconfig wlan0 up 10.20.30.41 netmask 255.255.255.252 broadcast 10.20.30.43
         *      - sudo dnsmasq
         *  - TODO supprimer le passphrasewpa (open wifi)
         *  - interface html/js pour submit le ssid
     */
    enable() {
        var self = this;
        this._enabled = true;
        let mac_address = this.getMacAddress()
        //displayHost = spawn('python3', ["-u", path.join(__dirname, 'main.py'), "-h", self._ssid , self._passphrase]);
        return new Promise( async function(resolve, reject) {
            // On désactive la connexion auto au wifi (wpa_supplicant)
            // On active l'ip du hotspot (ifconfig & dnsmasq)

            try {
                await file.saveFile(`/etc/dnsmasq.conf`, `interface=${self._interface}\nlisten-address=10.10.10.10\nbind-interfaces\nserver=8.8.8.8\ndomain-needed\nbogus-priv\nno-resolv\ndhcp-range=10.10.10.11,10.10.10.62,1h\n`)
                
                console.log("Config DHCP sauvegardée");

                    exec(`systemctl stop wpa_supplicant@${self._interface} && ifconfig ${self._interface} up ${ip} netmask ${netmask} broadcast ${broadcast} && dnsmasq`, { shell: '/bin/bash' }, (errIp) => {
                    //exec(`ifconfig ${self._interface} up ${ip} netmask ${netmask} broadcast ${broadcast} && dnsmasq`, { shell: '/bin/bash' }, (errIp) => {
                    if (!errIp) {
                        // On active le hotspot
                        var hostapdOptions = {
                            channel: 6,
                            driver: 'nl80211',
                            hw_mode: 'g',
                            interface: self._interface,
                            ssid: self._ssid,
                            wpa: 2,
                            wpa_passphrase: self._passphrase
                        };
                        
                        hostapd.enable(hostapdOptions, (errHostapd) => {
                            if (!errHostapd) {
                                try{
                                    serverlisten.close()
                                    app.close()
                                } catch(err){ console.log("server already closed----------------------------------------------") }
                                app.prepare().then(() => {
                                        server.use(bodyParser.json())
                                        serverlisten = server.listen(port, ip, 511, (req, res) => {
                                            console.log(`Listening on *: ${port}`);
                                            resolve({ ssid: self._ssid, passphrase: self._passphrase });
                                        })
                                        server.get('/', async (req, res) => {
                                            console.log('pass')
                                            app.render(req, res, '/', {hello: "receive"})
                                        }),
                                        server.get('/wifi', async (req, res) => {
                                            let networks = await self.scanNetworks()
                                            console.log("networks", networks)
                                            app.render(req, res, '/wifi', {networks})
                                        })
                                        server.post('/sendinfo', async (req, res) => {
                                            let dataWifi = ""
                                            const { ssid, password } = req.body
                                            console.log('WIFIDATA : ', ssid, password)
                                            if(password == ""){
                                                dataWifi = `ctrl_interface=/run/wpa_supplicant\nupdate_config=1\nnetwork={\n    ssid="${ssid}"\n    key_mgmt=NONE\n}`;
                                            } else {
                                                dataWifi = `ctrl_interface=/run/wpa_supplicant\nupdate_config=1\nnetwork={\n    ssid="${ssid}"\n    psk="${password}"\n}`;
                                            }
                                            Promise.all([await file.saveFile(`/etc/wpa_supplicant/wpa_supplicant-wlan0.conf`, dataWifi)])
                                            
                                            await self.disable()
                                            exec("reboot", { shell: '/bin/bash' });
                                            /*internetAvailable({
                                                timeout: 15000,
                                                retries: 13
                                            }).then(() => {
                                                //online
                                                displayHost.kill()
                                                displayMac = spawn('python3', ["-u", path.join(__dirname, 'main.py'), "-m", mac_address]);
                                            }).catch(() => {
                                                //ofline
                                                res.send({online : false})
                                            });*/
                                        })
                                        server.get('*', (req, res) => {
                                            return handle(req, res)
                                        })
                                })
                            } else {
                                console.log("errHostapd", errHostapd)
                                reject(errHostapd);
                            }
                        });
                    } else {
                        reject(errIp);
                    }
                });
            } catch(err){
                console.log(err)
            }
                
        });
    }

    /**
         * Désactive le réseau Wifi de la box
         * - inverser
         *      - sudo ifconfig wlan0 up 10.20.30.41 netmask 255.255.255.252 broadcast 10.20.30.43
         *      - sudo dnsmasq
         * - activer wpa_supplicant
         * - se connecter au wifi
     */
    disable() {
        return new Promise((resolve, reject) => {
            // On kill les 2 processus
            const self = this
            console.log("wii disabled WIFI")
                exec("killall dnsmasq hostapd", { shell: '/bin/bash' }, () => {
                //hostapd.disable('wlan0', function(err) {
                    wpa_supplicant.disable('wlan0', (err) => {});
                    try {
                    execSync(`ifconfig ${self._interface} down && ifconfig ${self._interface} up && systemctl start wpa_supplicant@${self._interface} && systemctl enable wpa_supplicant@${self._interface}`, { shell: '/bin/bash' });
                } catch (err) {
                    console.log(err.status, err.message, err.stderr, err.stdout);
                    reject(err);
                }
                resolve();
            });
        });
    }

    getMacAddress(){
        return new Promise(function(resolve, reject) {
            exec('cat /sys/class/net/eth0/address', function(err, stdout, stderr) {
                if (err) reject(err);
                resolve(stdout.toUpperCase().trim());
            });
        });
    }

    resume(){
        const self = this;
        return new Promise((resolve, reject) => {
            resolve({ssid : self._ssid, passphrase : self._passphrase, interface : self._interface})
        })
    }

    scanNetworks(){
        const self = this;
        return new Promise((resolve, reject) => {
            try{
                iw.scan({ iface : self._interface, show_hidden : true }, (err, networks) => {
                    if(!err){
                        resolve(networks)
                    }
                })
            } catch(err){
                reject(err)
            }
        })
    }

}
module.exports = Wifi
