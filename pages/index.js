import {useState, useEffect} from 'react';
import { Button, Menu, Text, Pane, Heading, Icon } from 'evergreen-ui'
import Link from 'next/link'
import { useRouter } from 'next/router'


const Choices = (props) => {
  const router = useRouter()

  useEffect(() => {
  })
  return(
    <>
    <Pane display="flex" padding={16} background="tint2" borderRadius={3}>
      <Pane flex={1} alignItems="center" display="flex" justifyContent="center">
        <Heading size={600}>Menu</Heading>
      </Pane>
    </Pane>
      <Menu>
        <Menu.Group>
          <Menu.Item><a href="/wifi"><Text size={600}>Wifi connection</Text></a></Menu.Item>
        </Menu.Group>
      </Menu>
    </>
  );
}

export default Choices;