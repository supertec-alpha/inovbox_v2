import React, {Component} from 'react'


export default class Register extends Component{

    constructor(){
        super();

        this.state = {
            loading: false
        }
    }

    static async getInitialProps ({ req, query }) {
        return{query}
      }

    render(){
        const {loading} = this.state
        const {mac} = this.props.query
        console.log("Register render props", this.props)
        
        return(
            <div id="mac_adress">
                <h2>{mac && mac}</h2>
            </div>
        );

    }
}