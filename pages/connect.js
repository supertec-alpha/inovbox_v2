import {Component} from 'react';


export default class Connect extends Component{
    
    constructor(){
        super();
        this.state = {
            loading : false,
            submitted : false,
            wifi : {
              ssid : "",
              password: ""
            }
        }
    }

    componentDidMount = () => {
      this.setState({ wifi : { ssid : this.props.url.query.ssid }})
    }

    submitForm = (data) => {
        fetch('/sendinfo', {
          method: 'post',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        })
      }

    render = () => {
        return (
          <form className="wifi-form" onSubmit={e => {
              e.preventDefault()
            }}>
              <div className="">
                <div className="">
                  <input name="ssid" label="ssid" value={this.state.wifi.ssid} disabled />
                </div>
                <div className="">
                  <input name="password" label="password" type='password' value={this.state.wifi.password} onChange={(e) => this.setState({ wifi : {...this.state.wifi, password : e.target.value} })} required />
                </div>
              </div>

              <button 
                  type="button"
                  className="buttonSubmit"
                  onClick={() => {this.submitForm(this.state.wifi)}}>
                Submit
              </button>
            </form>
        )
    }
}