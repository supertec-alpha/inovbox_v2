import {useState, useEffect} from 'react';
import { Spinner, Pane, Table, Heading, Icon, Dialog, TextInput, Button, SideSheet, Position } from 'evergreen-ui'
import Link from 'next/link'
import { useRouter } from 'next/router'
const utf8 = require('utf8');

const Wifi = (props) => {
    const router = useRouter()

    const [loading, setLoading] = useState(false)
    const [networks, setNetworks] = useState([])
    const [isShown, setShown] = useState(false)
    const [valid, setValid] = useState(false)
    const [wifiData, setWifiData] = useState({ ssid:'', password: ''})

    console.log("props Wifi", props)
    useEffect(() => {
        getNetworks()
    }, [])

    const utf8Decode = (str) => {
        str = str.replace(/\\x[\dA-F]{2}/ig, function(match) {
            return String.fromCharCode(parseInt(match.replace(/\\x/g, ''), 16));
        });
        return str;
    }

    const getNetworks = () => {
        console.log('network')
        setLoading(true)
        //console.log('Props', props.url.query.networks)
        setNetworks(router.query.networks)
        setLoading(false)

    }

    const submitForm = (data) => {
        fetch('/sendinfo', {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
        },
            body: JSON.stringify(data)
        })
    }

    const validModal = () => (
        <Pane alignItems="center" justifyContent="center">
        <Dialog
            isShown={valid}
            onCloseComplete={() => {}}
            hasFooter={false}
        >
            <Icon size={40} icon="tick-circle" color="success" />
        </Dialog>
        </Pane>
    )

    
    return (
        <div>
            {loading || !networks.length > 0 && (
                <Pane>
                    {/* <Spinner marginX="auto" marginY={120} /> */}
                </Pane> )
            }
            <Pane>
                {networks.length > 0 && (
                    <>
                    <Pane display="flex" padding={16} background="tint2" borderRadius={3}>
                        <Pane marginRight={16} alignItems="center" display="flex">
                                <a href='/'><Icon icon="arrow-left" /></a>
                        </Pane>
                        <Pane flex={1} alignItems="center" display="flex">
                            <Heading size={600}>Wifi list</Heading>
                        </Pane>
                    </Pane>
                    {networks.map(network => {
                        return (
                            <>
                            <Table.Row
                                key={network.address}
                                isSelectable
                                onSelect={() => {
                                    setWifiData({...wifiData, ssid: utf8.decode(utf8Decode(network.ssid))})
                                    setShown(true)
                                }}
                            >
                                <Table.TextCell>{utf8.decode(utf8Decode(network.ssid))}</Table.TextCell>
                            </Table.Row>
                        </>
                        )
                    })}
                    </>
                )}
                {validModal()}
            </Pane>
            <SideSheet isShown={isShown} position={Position.BOTTOM} onCloseComplete={() => {setWifiData({...wifiData, password : ""}); setShown(false)}}>
                <form className="wifi-form" onSubmit={e => {
                    e.preventDefault()
                }}>
                    <Pane alignItems="center" justifyContent="center" padding={30}>
                        <TextInput height={40} value={wifiData.ssid} disabled marginBottom={16}/>
                        <TextInput placeholder='Password' height={40} onChange={(e) => { setWifiData({...wifiData, password : e.target.value})}} value={wifiData.password} />
                    </Pane>
                    <Pane display="flex" alignItems="center" justifyContent="center" marginBottom={30}>
                        <Button  onClick={() => {
                            submitForm(wifiData) 
                            setShown(false)
                            setValid(true)
                            }}>Connect
                        </Button>
                    </Pane>
                </form>
            </SideSheet>
        </div>
    )
}

Wifi.getInitialProps = async ({req}) => {
    return {receive : true}
}

export default Wifi