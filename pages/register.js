import React, {Component} from 'react'
import Loading from '../components/loading';


export default class Register extends Component{

    constructor(){
        super();

        this.state = {
            loading: true
        }
    }

    static async getInitialProps ({ req, query }) {
        return{query}
      }

      componentDidMount(){
          console.log('Mount REGISTER')
          console.log('props', this.props)
          this.setState({
              loading: false
          })
          
      }

    render(){
        const {loading} = this.state
        const {box} = this.props.query
        console.log("Register render props", this.props)
        if(loading) return <Loading />

        
        return(
            <div id="register">
                <h2>{box && box.ssid && box.ssid}</h2>
                <h2>{box && box.passphrase && box.passphrase}</h2>
            </div>
        );

    }
}