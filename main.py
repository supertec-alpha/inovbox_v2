import pygame
from pygame.locals import *
from sys import argv
import sys


import os
#os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (1650, 10)

def main():

    pygame.init()
    infoScreen = pygame.display.Info()

    X = infoScreen.current_w
    Y = infoScreen.current_h

    print(infoScreen)

    white = (255, 255, 255)
    black = (0,0,0,0)

    display_surface = pygame.display.set_mode((X, Y), NOFRAME)
    pygame.mouse.set_visible(False)
    #background = pygame.image.load("img/bg.jpg").convert()
    #rotated_bg = pygame.transform.rotate(background, 0)
    #display_surface.blit(background,(0,0))

    def displayMacAdress(mac):
        font = pygame.font.SysFont("Arial", 92)
        data = font.render(mac, True, white)
        rotated_text = pygame.transform.rotate(data, 90)
        textRect = rotated_text.get_rect()
        textRect.center = (X // 2, Y // 2)
        background = pygame.image.load("data/images/register.jpg").convert()
        rotated_bg = pygame.transform.rotate(background, 90)
        resize_bg = pygame.transform.scale(rotated_bg, (X, Y))
        display_surface.blit(resize_bg,(0,0))
        #display_surface.fill(white)
        display_surface.blit(rotated_text, textRect)

        pygame.display.update()

    def displayDownloadPlay():
        display_surface = pygame.display.set_mode((100,300), NOFRAME)
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (X * 0.93, Y // 9 )
        font = pygame.font.SysFont("Arial", 32)
        data = font.render("DOWNLOADING...", True, black)
        rotated_text = pygame.transform.rotate(data, 90)
        textRect = rotated_text.get_rect()
        textRect.center = (100 // 2, 300 // 2)

        display_surface.fill(white)
        display_surface.blit(rotated_text, textRect)

        pygame.display.update()

    def displayDownload():
        font = pygame.font.SysFont("Arial", 92)
        data = font.render("DOWNLOADING...", True, white)
        rotated_text = pygame.transform.rotate(data, 90)
        textRect = data.get_rect()
        textRect.center = (X // 2, Y // 2)

        background = pygame.image.load("data/images/loading.jpg").convert()
        rotated_bg = pygame.transform.rotate(background, 90)
        resize_bg = pygame.transform.scale(background, (X, Y))
        display_surface.blit(background,(0,0))
        #display_surface.fill(white)
        display_surface.blit(data, textRect)

        pygame.display.update()

    def displayHostInfo(ssidHost, passwd):
        font = pygame.font.SysFont(None, 82, bold=False)
        ssid = font.render(ssidHost, True, white)
        passd = font.render(passwd, True, white)
        rotated_text_ssid = pygame.transform.rotate(ssid, 90)
        rotated_text_pass = pygame.transform.rotate(passd, 90)        
        ssidRect = rotated_text_ssid.get_rect()
        ssidRect.center = (X // 2, Y // 2)
        passwdRect = rotated_text_pass.get_rect()
        passwdRect.center = (X // 2 + 100, Y // 2)
        background = pygame.image.load("data/images/register.jpg").convert()
        rotated_bg = pygame.transform.rotate(background, 90)
        resize_bg = pygame.transform.scale(rotated_bg, (X, Y))
        display_surface.blit(resize_bg,(0,0))

        #display_surface.fill(white)
        display_surface.blit(rotated_text_ssid, ssidRect)
        display_surface.blit(rotated_text_pass, passwdRect)

        pygame.display.update()

    def displayImage(url, rotation):
        image = pygame.image.load(url).convert()
        rotated_image = pygame.transform.rotate(image, -rotation)
        imageRect = rotated_image.get_rect ()
        imageRect.center = (X // 2, Y // 2)
        display_surface.blit(rotated_image, imageRect)
        pygame.display.update()

    def displayLoader(rotation):
        font = pygame.font.Font(None, 92)
        data = font.render("LOADING...", True, white)
        rotated_text = pygame.transform.rotate(data, -rotation)
        textRect = data.get_rect()
        textRect.center = (X // 2, Y // 2)
        if rotation == 180 or rotation == 0:
            background = pygame.image.load("data/images/loading_landscape.jpg").convert()
        else:
            background = pygame.image.load("data/images/loading.jpg").convert()
        rotated_bg = pygame.transform.rotate(background, -rotation)
        resize_bg = pygame.transform.scale(rotated_bg, (X, Y))
        display_surface.blit(resize_bg,(0,0))

        #display_surface.fill(white)
        #display_surface.blit(data, textRect)

        pygame.display.update()

    while True:
        if argv[1] == '-m':
            displayMacAdress(argv[2])
        elif argv[1] == '-h':
            displayHostInfo(argv[2],argv[3])
        elif argv[1] == '-i':
            displayImage(argv[2], int(argv[3]))
        elif argv[1] == '-l':
            displayLoader(int(argv[2]))
        elif(argv[1] == '-d'):
            displayDownload()
        elif(argv[1] == '-t'):
            displayDownloadPlay()

        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                quit() 


if __name__ == "__main__":
    main()